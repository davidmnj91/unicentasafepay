# README #

This README would normally document whatever steps are necessary to get your application up and running.



### What is this repository for? ###

* Quick summary
This our implementation of Unicenta 3.91.3 with extended capabilities, adding support to drive the Safepay CR2 & NR2 cash handler. This version adds new feature as managing providers payment in cash, managing Notes Cassette collection, reports of CR & NR levels.

* Version
Unicenta 3.95SP

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### How do I get set up? ###

* Summary of set up
A fresh installation of Unicenta 3.91.3 is needed & running (DB created).


* Configuration
Tested DBs are MySQL or PostgreSQL, but we strongly recommend to use PostgreSQL.
The JNI librairies  for Windows with Java 32 bits cccjni.dll, ccc.dll or for Linux with java 32 bits cccjni.so & ccc.so, unicentaopos.jar and safepay-2.3.jar are provided with this new version of Unicenta SP.



* Dependencies
The Gunnebo JNI librairy is need in order to get the safepay-2.3.jar working.


* Database configuration
Once the new Unicenta (with all the necessary libs is created) a SAFEPAY table will be added to the DB at first start.

* How to run tests
No simulator is available, so you need a Safepay CR & NR available to complete all the tests
JUnit tests will be implement later

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact