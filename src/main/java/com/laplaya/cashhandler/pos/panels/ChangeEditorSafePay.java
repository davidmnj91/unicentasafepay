//    uniCenta oPOS  - Touch Friendly Point Of Sale
//    Copyright (c) 2009-2014 uniCenta & previous Openbravo POS works
//    http://www.unicenta.com
//
//    This file is part of uniCenta oPOS
//
//    uniCenta oPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   uniCenta oPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with uniCenta oPOS.  If not, see <http://www.gnu.org/licenses/>.

package com.laplaya.cashhandler.pos.panels;

import com.laplaya.cashhandler.fullprotocol.SafePay;
import com.openbravo.basic.BasicException;
import com.openbravo.data.gui.MessageInf;
import com.openbravo.data.user.DirtyManager;
import com.openbravo.data.user.EditorRecord;
import com.openbravo.format.Formats;
import com.openbravo.pos.forms.AppConfig;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.AppView;
import com.openbravo.pos.forms.DataLogicSystem;
import com.openbravo.pos.scripting.ScriptEngine;
import com.openbravo.pos.scripting.ScriptException;
import com.openbravo.pos.scripting.ScriptFactory;
import com.openbravo.pos.util.ThumbNailBuilder;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

/**
 *
 * @author ADMS
 */
public final class ChangeEditorSafePay extends javax.swing.JPanel implements EditorRecord, Observer {
    
    private String m_sId;
    private String m_sPaymentId;
    private Date datenew;
   
    private final AppView m_App;
    
    private final SafePay SP;
    private final HashMap<Double,Integer> ChangeFormat = new HashMap<>();
    private TreeMap<Double,Integer> Currencies;
    private double m_dPaid, m_dChange;
    private final DirtyManager m_dirty;
        
    private final DataLogicSystem dlSystem;
    
    PropertyChangeListener coins = (PropertyChangeEvent evt) -> {updateCoins();};
    PropertyChangeListener start = (PropertyChangeEvent evt) -> {IOChange();};
    
    private static final Logger logger = Logger.getLogger("UNICENTA");
        
    /** Creates new form JPanelChange
     * @param oApp
     * @param dirty */
    public ChangeEditorSafePay(AppView oApp, DirtyManager dirty) {
        m_App = oApp;
        dlSystem = (DataLogicSystem) m_App.getBean("com.openbravo.pos.forms.DataLogicSystem");
        m_dirty = dirty;
        
        logger.log(Level.INFO, "{0} SafePay Transaction type: 4", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
        SP = SafePay.getInstance(m_App,4);
        
        initComponents();
        jGiven.addPropertyChangeListener("text", m_dirty);
        
        jGiven.addPropertyChangeListener("text", coins);
        jChange.addPropertyChangeListener("text", coins);
        jGiven.addPropertyChangeListener("enabled", start);
        
        AppConfig m_config =  new AppConfig(new File((System.getProperty("user.home")), AppLocal.APP_ID + ".properties"));        
        m_config.load();
        
        writeValueEOF();
    }

    @Override
    public void writeValueEOF() {
        ChangeFormat.clear();
        m_dPaid = 0;
        m_dChange = 0;
        m_sId = null;
        m_sPaymentId = null;
        datenew = null;
        jGiven.setText(Formats.CURRENCY.formatValue(m_dPaid));
        jGiven.setEnabled(false);
        jChange.setText(Formats.CURRENCY.formatValue(m_dChange));
        jChange.setEnabled(false);
        jMoney.setText(ChangeFormat.toString());
        jMoney.setEnabled(false);
        repaint();
    }

    @Override
    public void writeValueInsert() {
        ChangeFormat.clear();
        m_dPaid = 0;
        m_dChange = 0;
        m_sId = null;
        m_sPaymentId = null;
        datenew = null;
        jGiven.setEnabled(true);
        jChange.setEnabled(true);
        jMoney.setEnabled(true);
        SP.RegisterObserver((Observer)this);
    }
    
    
    @Override
    public void writeValueDelete(Object value) {}

    @Override
    public void writeValueEdit(Object value) {
        CancelPayment();
        writeValueEOF();
    }
    
    /**
     *
     * @return
     * @throws BasicException
     */
    @Override
    public Object createValue() throws BasicException {
        Object[] payment = new Object[8];
        payment[0] = m_sId == null ? UUID.randomUUID().toString() : m_sId;
        payment[1] = m_App.getActiveCashIndex();
        payment[2] = datenew == null ? new Date() : datenew;
        payment[3] = m_sPaymentId == null ? UUID.randomUUID().toString() : m_sPaymentId;
        payment[4] = "change";
        payment[5] = 0.0;
        payment[6] = m_App.getAppUserView().getUser().getId();
        payment[7] = Formats.CURRENCY.formatValue(m_dPaid)+ ": " + ChangeFormat.toString();

        if(!executeChange()){
            jGiven.setEnabled(false);
            return new Object[9];
        }
        jGiven.setEnabled(false);
        return payment;
    }
    
    @Override
    public boolean checkFields(){
        return true;
    }
    
    private void IOChange(){
        if(jGiven.isEnabled()){
            payIn();
        }else {
            SP.Close();
        }
    }
       
    private void payIn() {
        SP.Open();
        this.jSPInfo.setText(SP.Check());
        Currencies = new TreeMap<>(SP.CurrenciesTracking());
        SP.startCollecting();
    }
    
    private boolean executeChange(){
        SP.endCollecting();
        m_dPaid = SP.getDeposited();
        SP.RemoveObserver();
        if((SP.canDispense(m_dChange,getChangeArray(ChangeFormat))) && (SP.canDispense(SP.roundDouble(m_dPaid - m_dChange), null))){
            SP.Dispense(m_dChange, getChangeArray(ChangeFormat));
            if(SP.roundDouble(m_dPaid - m_dChange) > 0) SP.Dispense(SP.roundDouble(m_dPaid - m_dChange), null);
            SP.Close();
            return true;
        }
        SP.Dispense(m_dPaid, null);
        SP.Close();
        return false;
    }
    
    private void CancelPayment(){
        SP.abortCollecting();
        SP.Destroy();
    }
    
    @Override
    public Component getComponent() {
        return this;
    }
    
    @Override
    public void update(Observable o, Object arg) {
        printState();
    }
    
    private void printState(){
        SP.ManageErrors();
        m_dPaid = SP.getDeposited();
        this.jGiven.setText(Formats.CURRENCY.formatValue(m_dPaid)); 
        this.jChange.setText(Formats.CURRENCY.formatValue(m_dChange));
        this.jMoney.setText(ChangeFormat.toString());
        repaint();
        revalidate();
    }
    
    @Override
    public void refresh() {    }
    
    private String getChangeArray(HashMap change){
        Map<Double,Integer> map = new TreeMap<>(change);
        String array = "";
        array = map.entrySet().stream().map((entry) -> String.valueOf(entry.getKey()*100)+":"+String.valueOf(entry.getValue())+",").reduce(array, String::concat);
        if(!"".equals(array)) return array.substring(0, array.length()-1);
        return null;
    }
    
    private void updateCoins(){
        jPanel6.removeAll();
        String code = dlSystem.getResourceAsXML("payment.cash");
        if (code != null) {
            try {
                ScriptEngine script = ScriptFactory.getScriptEngine(ScriptFactory.BEANSHELL);
                script.put("payment", new ChangeEditorSafePay.ScriptPaymentCash(dlSystem));    
                script.eval(code);
            } catch (ScriptException e) {
                MessageInf msg = new MessageInf(MessageInf.SGN_NOTICE, AppLocal.getIntString("message.cannotexecute"), e);
                msg.show(this);
            }
        }
    } 
    /**
     *
     */
    public class ScriptPaymentCash {
        
        private final DataLogicSystem dlSystem;
        private final ThumbNailBuilder tnbbutton;
        private final AppConfig m_config;
        
        /**
         *
         * @param dlSystem
         */
        public ScriptPaymentCash(DataLogicSystem dlSystem) {
//added 19.04.13 JDL        
            AppConfig m_config =  new AppConfig(new File((System.getProperty("user.home")), AppLocal.APP_ID + ".properties"));        
            m_config.load();
            this.m_config = m_config;
        
            this.dlSystem = dlSystem;
            tnbbutton = new ThumbNailBuilder(64, 50, "com/openbravo/images/cash.png");
        }
        
        /**
         *
         * @param image
         * @param amount
         */
        public void addButton(String image, double amount) {
            JButton btn = new JButton();
//added 19.04.13 JDL removal of text on payment buttons if required.   
            try {
            if ((m_config.getProperty("payments.textoverlay")).equals("true")){
                     btn.setIcon(new ImageIcon(tnbbutton.getThumbNailText(dlSystem.getResourceAsImage(image),"")));  
            } else {
                     btn.setIcon(new ImageIcon(tnbbutton.getThumbNailText(dlSystem.getResourceAsImage(image), Formats.CURRENCY.formatValue(amount)))); 
            }
            } catch (Exception e){
                    btn.setIcon(new ImageIcon(tnbbutton.getThumbNailText(dlSystem.getResourceAsImage(image), Formats.CURRENCY.formatValue(amount))));        
            }   
            if((amount > SP.roundDouble(m_dPaid - m_dChange)) || !canDisplay(amount)) btn.setEnabled(false);
            btn.setFocusPainted(false);
            btn.setFocusable(false);
            btn.setRequestFocusEnabled(false);
            btn.setHorizontalTextPosition(SwingConstants.CENTER);
            btn.setVerticalTextPosition(SwingConstants.BOTTOM);
            btn.setMargin(new Insets(2, 2, 2, 2));
            btn.addActionListener(m_dirty);
            btn.addActionListener(new AddAmount(amount));
            jPanel6.add(btn);  
        }
        
        private boolean canDisplay(double amount){
            if(!Currencies.containsKey(amount)) return false;
            if(Currencies.containsKey(amount) && ChangeFormat.containsKey(amount)){
                return Currencies.get(amount) > ChangeFormat.get(amount);
            }
            return true;
        }
    }
    
    private class AddAmount implements ActionListener {        
        private final double amount;
        public AddAmount(double amount) {
            this.amount = amount;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            int count = ChangeFormat.containsKey(amount) == false ? 0 : ChangeFormat.get(amount);
            ChangeFormat.put(amount, ++count);
            m_dChange += amount;
            printState();
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jGiven = new javax.swing.JLabel();
        jChange = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jButtonClear = new javax.swing.JButton();
        jMoney = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jSPInfo = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        jLabel8.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel8.setText(AppLocal.getIntString("Label.InputCash")); // NOI18N
        jLabel8.setPreferredSize(new java.awt.Dimension(100, 30));

        jLabel6.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel6.setText(AppLocal.getIntString("Label.ChangeCash")); // NOI18N
        jLabel6.setPreferredSize(new java.awt.Dimension(100, 30));

        jGiven.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jGiven.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jGiven.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        jGiven.setOpaque(true);
        jGiven.setPreferredSize(new java.awt.Dimension(180, 30));

        jChange.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        jChange.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jChange.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        jChange.setOpaque(true);
        jChange.setPreferredSize(new java.awt.Dimension(180, 30));

        jPanel6.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), null));
        jPanel6.setMaximumSize(new java.awt.Dimension(220, 200));
        jPanel6.setMinimumSize(new java.awt.Dimension(220, 200));
        jPanel6.setPreferredSize(new java.awt.Dimension(220, 200));
        jPanel6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jButtonClear.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButtonClear.setText("X");
        jButtonClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonClearActionPerformed(evt);
            }
        });

        jMoney.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jMoney.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), null));

        jLabel7.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel7.setText(AppLocal.getIntString("Label.ChangeFormat")); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(100, 30));

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setText("SafePay Info:");

        jSPInfo.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jGiven, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jChange, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSPInfo)))
                .addGap(118, 118, 118)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonClear, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jGiven, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jChange, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(48, 48, 48)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSPInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(278, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonClear, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 174, Short.MAX_VALUE))
        );

        add(jPanel3, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonClearActionPerformed
        ChangeFormat.clear();
        m_dChange = 0;
        printState();
    }//GEN-LAST:event_jButtonClearActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonClear;
    private javax.swing.JLabel jChange;
    private javax.swing.JLabel jGiven;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jMoney;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JLabel jSPInfo;
    // End of variables declaration//GEN-END:variables
    
}
