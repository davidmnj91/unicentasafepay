/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.laplaya.cashhandler.pos.panels;

import com.laplaya.cashhandler.fullprotocol.SafePay;
import com.openbravo.format.Formats;
import com.openbravo.pos.forms.AppView;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;

/**
 *
 * @author ADMS
 */
public class SafePayAdministration {
    
    ChartFrame CP;
    HashMap<String,Frame> Components;
    private SafePay SP;
    private final AppView m_app;
    
    public SafePayAdministration(AppView app){
        m_app = app;
    }
    
    private void init() {
        if(SP == null){
            SP = SafePay.getInstance(m_app,5);
            SP.Open();
        }
    }
    
    public void OpenCover(){
        init();
        SP.OpeningCover();
        SP.Destroy();
    }
    
    public void MoneyInfo(){
        init();
        Map<Double,Integer> money = new TreeMap<>(SP.CurrenciesTracking());
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        money.entrySet().stream().forEach((mon) -> {
            dataset.setValue(mon.getValue(),"Amounts",Formats.CURRENCY.formatValue(mon.getKey()));
        });
        
        JFreeChart chart = ChartFactory.createBarChart("Money Info", "Coin Value", "Amount", dataset, PlotOrientation.VERTICAL, true, true, false);
        CategoryPlot p = chart.getCategoryPlot();
        BarRenderer renderer = (BarRenderer)p.getRenderer();
        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        renderer.setItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}",decimalFormat));
        p.setRenderer(renderer);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12,TextAnchor.CENTER));
        renderer.setItemLabelsVisible(true);
        chart.getCategoryPlot().setRenderer(renderer);
        
        CP = new ChartFrame("Money Info", chart);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        CP.setSize(800, 500);
        CP.setLocation((dim.width-CP.getSize().width)/2, (dim.height-CP.getSize().height)/2);
        CP.setVisible(true);
        SP.Destroy();
    }
    
    public void showErrorTransactions(){
        JTransactionsFinder finder = JTransactionsFinder.getReceiptFinder(null, m_app);
        finder.setVisible(true);
    }
    
    public void showSafePayMessages(){
        init();
        JOptionPane.showMessageDialog(null, SP.Check(), "INFO:", JOptionPane.INFORMATION_MESSAGE); 
        SP.Destroy();
    }
    
    public void ReleaseCassete(){
        init();
        SP.ReleaseCassete();
        SP.Destroy();
    }
    
    public void NoteTransfer(){
        init();
        String[] choices = { "All", "Base Level", "Work Level", "Amount" };
        String input = (String) JOptionPane.showInputDialog(null, "Note Transfer",
            "How Much??", JOptionPane.QUESTION_MESSAGE, null, choices, choices[1]);
        short value = 5;
        switch(input){
            case "All": {
                value = 0;
                break;
            }
            case "Base Level": {
                value = 1;
                break;
            }
            case "Work Level": {
                value = 2;
                break;
            }
            case "Amount": {
                value = 3;
                break;
            }
        }
        SP.NoteTransfer(value);
        SP.Destroy();
    }
    
    
}