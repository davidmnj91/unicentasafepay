/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.laplaya.cashhandler.pos.panels;

import com.laplaya.cashhandler.fullprotocol.SafePay;
import com.openbravo.format.Formats;
import com.openbravo.pos.customers.CustomerInfoExt;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.AppView;
import com.openbravo.pos.forms.DataLogicSystem;
import com.openbravo.pos.payment.JPaymentInterface;
import com.openbravo.pos.payment.JPaymentNotifier;
import com.openbravo.pos.payment.PaymentInfo;
import com.openbravo.pos.payment.PaymentInfoCash_original;
import java.awt.Component;
import com.openbravo.pos.util.RoundUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMS
 */
public class JPaymentSafePay extends javax.swing.JPanel implements JPaymentInterface, Observer {

    private final JPaymentNotifier m_notifier;
    private double m_dPaid, m_dInserted;
    private double m_dTotal;
    private final SafePay SP;
        
    AppView m_app;
    private static final Logger logger = Logger.getLogger("UNICENTA");
    /**
     * Creates new form JPaymentCashHandlerPanel
     * @param notifier
     * @param dlSystem
     * @param app
     */
    public JPaymentSafePay(JPaymentNotifier notifier, DataLogicSystem dlSystem, AppView app) {
        m_notifier = notifier;
        m_app = app;
        
        logger.log(Level.INFO, "{0} SafePay Transaction type: 0", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
        SP = SafePay.getInstance(m_app,0);
        
        initComponents();
    }
    
        /**
     *
     * @param customerext
     * @param dTotal
     * @param transID
     */
    @Override
    public void activate(CustomerInfoExt customerext, double dTotal, String transID) {
        SP.RegisterObserver((Observer)this);
        m_dTotal = dTotal;
        m_dPaid = 0;
        SP.Open();
        this.jSPInfo.setText(SP.Check());
        SP.startCollecting();
    }
    
    @Override
    public void deactivate(){
        if(SP != null) {
            SP.abortCollecting();
            m_dPaid = 0;
            SP.Close();
        }
    }
    
    @Override
    public PaymentInfo executePayment() {
        logger.log(Level.INFO, "{0} Execute payment button pressed, thread: " + Thread.currentThread().getName(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
        SP.endCollecting();
        m_dPaid = SP.getDeposited();
        m_dInserted += m_dPaid;
        if (m_dPaid - m_dTotal >= 0.0) {
            logger.log(Level.INFO, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + " Execute Payment: {0}", (m_dPaid - m_dTotal));
            SP.RemoveObserver();
            if(SP.canDispense((m_dPaid - m_dTotal),null)){
                SP.Dispense((m_dPaid - m_dTotal),null);
            } else{
                SP.Dispense(m_dInserted, null);
                SP.RegisterObserver((Observer)this);
                SP.startCollecting();
                return null;
            }
            SP.Destroy();
            // pago completo
            m_dInserted = 0;
            return new PaymentInfoCash_original(m_dTotal, m_dPaid);
        } else {
            SP.Close();          
            return new PaymentInfoCash_original(m_dPaid, m_dPaid);
        }  
    }
    
    @Override
    public boolean cancelPartPayment(double amount) { 
        SP.abortCollecting();
        logger.log(Level.INFO, "CancelPartPayment: {0}", m_dInserted);
        SP.Dispense(amount, null);
        SP.Destroy();
        m_dInserted -= amount;
        return true; 
    }
        
    @Override
    public void update(Observable o, Object arg) {
        UpdateGUI();
    }
    
    private void UpdateGUI(){
        SP.ManageErrors();
        m_dPaid = SP.getDeposited();
        int iCompare = RoundUtils.compare(m_dPaid, m_dTotal);
        
        m_jMoneyEuros.setText(Formats.CURRENCY.formatValue(m_dPaid));
        m_jChangeEuros.setText(iCompare > 0 
                ? Formats.CURRENCY.formatValue(m_dPaid - m_dTotal)
                : null); 
        m_notifier.setStatus(m_dPaid > 0.0, iCompare >= 0);
        repaint();
        revalidate();
    }
    
    @Override
    public String getName(){
        return AppLocal.getIntString("tab.SafePay");
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        m_jChangeEuros = new javax.swing.JLabel();
        m_jMoneyEuros = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSPInfo = new javax.swing.JLabel();

        addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
                formAncestorRemoved(evt);
            }
        });
        setLayout(new java.awt.BorderLayout());

        jPanel5.setLayout(new java.awt.BorderLayout());

        jPanel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel4.setMaximumSize(new java.awt.Dimension(0, 70));
        jPanel4.setOpaque(false);
        jPanel4.setPreferredSize(new java.awt.Dimension(0, 70));
        jPanel4.setLayout(null);

        m_jChangeEuros.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        m_jChangeEuros.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        m_jChangeEuros.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jChangeEuros.setOpaque(true);
        m_jChangeEuros.setPreferredSize(new java.awt.Dimension(180, 30));
        jPanel4.add(m_jChangeEuros);
        m_jChangeEuros.setBounds(120, 36, 180, 30);

        m_jMoneyEuros.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        m_jMoneyEuros.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        m_jMoneyEuros.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(javax.swing.UIManager.getDefaults().getColor("Button.darkShadow")), javax.swing.BorderFactory.createEmptyBorder(1, 4, 1, 4)));
        m_jMoneyEuros.setOpaque(true);
        m_jMoneyEuros.setPreferredSize(new java.awt.Dimension(180, 30));
        jPanel4.add(m_jMoneyEuros);
        m_jMoneyEuros.setBounds(120, 4, 180, 30);

        jLabel7.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel7.setText(AppLocal.getIntString("Label.ChangeCash")); // NOI18N
        jLabel7.setPreferredSize(new java.awt.Dimension(100, 30));
        jPanel4.add(jLabel7);
        jLabel7.setBounds(10, 36, 100, 30);

        jLabel9.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel9.setText(AppLocal.getIntString("Label.InputCash")); // NOI18N
        jLabel9.setPreferredSize(new java.awt.Dimension(100, 30));
        jPanel4.add(jLabel9);
        jLabel9.setBounds(10, 4, 100, 30);

        jPanel5.add(jPanel4, java.awt.BorderLayout.NORTH);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel1.setText("SafePay Info:");

        jSPInfo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSPInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(214, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSPInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(218, Short.MAX_VALUE))
        );

        jPanel5.add(jPanel1, java.awt.BorderLayout.CENTER);

        add(jPanel5, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void formAncestorRemoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_formAncestorRemoved
        m_dInserted = 0;
        m_dPaid = 0;      
        SP.RemoveObserver();
    }//GEN-LAST:event_formAncestorRemoved

    @Override
    public Component getComponent() {
        return this;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JLabel jSPInfo;
    private javax.swing.JLabel m_jChangeEuros;
    private javax.swing.JLabel m_jMoneyEuros;
    // End of variables declaration//GEN-END:variables
}