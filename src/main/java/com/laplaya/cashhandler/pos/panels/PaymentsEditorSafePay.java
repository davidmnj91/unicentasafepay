//    uniCenta oPOS  - Touch Friendly Point Of Sale
//    Copyright (c) 2009-2014 uniCenta & previous Openbravo POS works
//    http://www.unicenta.com
//
//    This file is part of uniCenta oPOS
//
//    uniCenta oPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   uniCenta oPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with uniCenta oPOS.  If not, see <http://www.gnu.org/licenses/>.

package com.laplaya.cashhandler.pos.panels;

import com.laplaya.cashhandler.fullprotocol.SafePay;
import com.openbravo.basic.BasicException;
import com.openbravo.data.gui.ComboBoxValModel;
import com.openbravo.data.loader.IKeyed;
import com.openbravo.data.user.DirtyManager;
import com.openbravo.data.user.EditorRecord;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.AppView;
import com.openbravo.pos.providers.DataLogicProviders;
import com.openbravo.pos.providers.JProviderFinder;
import com.openbravo.pos.providers.ProviderInfo;
import java.awt.Component;
import java.awt.event.ActionEvent;
import static java.lang.Math.abs;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMS
 */
public final class PaymentsEditorSafePay extends javax.swing.JPanel implements EditorRecord, Observer {
    
    private final ComboBoxValModel m_ReasonModel;
    private String m_sId;
    private String m_sPaymentId;
    private Date datenew;
    private String m_sNotes;
   
    private final AppView m_App;
    
    private final SafePay SP;
    double m_dPaid;
    
    private final DataLogicProviders dlProviders;   
    ProviderInfo provider;    
    
    private static final Logger logger = Logger.getLogger("UNICENTA");
    /** Creates new form JPanelPayments
     * @param oApp
     * @param dirty */
    public PaymentsEditorSafePay(AppView oApp, DirtyManager dirty) {
        m_App = oApp;
        dlProviders  = (DataLogicProviders) m_App.getBean("com.openbravo.pos.providers.DataLogicProviders");
        
        initComponents();
        
        logger.log(Level.INFO, "{0} SafePay Transaction type: 2", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
        SP = SafePay.getInstance(m_App,2);
       
        m_ReasonModel = new ComboBoxValModel();
        m_ReasonModel.add(new PaymentReasonPositive("cashin", AppLocal.getIntString("transpayment.cashin")));
        m_ReasonModel.add(new PaymentReasonNegative("cashout", AppLocal.getIntString("transpayment.cashout")));              
        m_jreason.setModel(m_ReasonModel);
        
        jTotal.addEditorKeys(m_jKeys);
        
        m_jreason.addActionListener(dirty);
        jTotal.addPropertyChangeListener("Text", dirty);
        m_jNotes.addPropertyChangeListener("Text", dirty);
        m_jNotes.addEditorKeys(m_jKeys);        
        m_jreason.addActionListener ((ActionEvent e) -> {IOChange();});
        
        writeValueEOF();
    }

    @Override
    public void writeValueEOF() {
        m_sId = null;
        m_sPaymentId = null;
        datenew = null;
        setReasonTotal(null, null);
        m_jreason.setEnabled(false);
        jTotal.reset();
        jTotal.setEnabled(false);
// JG Added July 2011
        m_sNotes = null;
        m_jNotes.setText(m_sNotes);
        m_jNotes.setEnabled(false);
        jChkBoxSelectPayment.setSelected(false);
        jChkBoxSelectPayment.setEnabled(false);
        jButtonProvider.setText(AppLocal.getIntString("Label.SelectProvider"));
        jButtonProvider.setEnabled(false);
    }

    @Override
    public void writeValueInsert() {
        m_dPaid = 0;
        m_sId = null;
        m_sPaymentId = null;
        datenew = null;
        setReasonTotal("cashin", null);
        m_jreason.setEnabled(true);
        jTotal.reset();
        jTotal.setEnabled(false);
        m_jKeys.setInactive(jTotal);
        m_sNotes = null;
        m_jNotes.setEnabled(true);
        jButtonProvider.setText(AppLocal.getIntString("Label.SelectProvider"));
        jButtonProvider.setEnabled(true);
        SP.RegisterObserver((Observer)this);
    }
    
    @Override
    public void writeValueDelete(Object value) {     }
    
    @Override
    public void writeValueEdit(Object value) {
        CancelPayment();
        writeValueEOF();
    }
    
    /**
     *
     * @return
     * @throws BasicException
     */
    @Override
    public Object createValue() throws BasicException {
        logger.log(Level.INFO, "{0} Execute Payment: {1}", new Object[]{new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()), jTotal.getDoubleValue()});
//JG Modified Array + 1 - July 2011
        Object[] payment = new Object[9];
        payment[0] = m_sId == null ? UUID.randomUUID().toString() : m_sId;
        payment[1] = m_App.getActiveCashIndex();
        payment[2] = datenew == null ? new Date() : datenew;
        payment[3] = m_sPaymentId == null ? UUID.randomUUID().toString() : m_sPaymentId;
        payment[4] = m_ReasonModel.getSelectedKey();
        PaymentReason reason = (PaymentReason) m_ReasonModel.getSelectedItem();
        Double dtotal = jTotal.getDoubleValue();
    //ADMS 29/05/2016
        SP.RemoveObserver();
        if(AppLocal.getIntString("transpayment.cashout").equals(m_jreason.getSelectedItem().toString())) {
            String Format = jChkBoxSelectPayment.isSelected() == true ? selectMoneyFormat(dtotal) : null;
            if(!SP.canDispense(dtotal, Format)){
                setReasonTotal(null, null);
                SP.Close();
                return new Object[9];
            } else {
                 payment[5] = reason.addSignum(SP.Dispense(dtotal, Format));
            }
        } else {
            SP.endCollecting();
            payment[5] = reason.addSignum(dtotal);
        }
    //Añadir Usuario a la base de datos y comprobar Comentario obligatorio
        payment[6] = m_App.getAppUserView().getUser().getId();
        String snotes = "";
        m_sNotes = m_jNotes.getText();
        payment[7] = m_sNotes == null ? snotes : m_sNotes;
        payment[8] = provider == null ? null : provider.getId();
        
        return payment;           
    }
    
    @Override
    public boolean checkFields(){
        return (m_jreason.getSelectedItem() != null) || (!("".equals(this.jTotal.getText())) || (this.jTotal.getText() != null) || !("".equals(this.m_jNotes.getText())) || (this.m_jNotes.getText() != null) || (!this.m_jNotes.getText().trim().isEmpty()) || !(AppLocal.getIntString("Label.SelectProvider").equals(this.jButtonProvider.getText())));
    }
    
    private void IOChange(){
        if (m_jreason.getSelectedItem() != null){
            if(AppLocal.getIntString("transpayment.cashin").equals(m_jreason.getSelectedItem().toString())){
                jChkBoxSelectPayment.setSelected(false);
                jChkBoxSelectPayment.setEnabled(false);
                payIn();
            }else if(AppLocal.getIntString("transpayment.cashout").equals(m_jreason.getSelectedItem().toString())) {
                jChkBoxSelectPayment.setEnabled(true);
                payOut();
            }
        }else {
            SP.Close();
        }
    }
    
    private void payIn() {
        m_dPaid = 0;
        SP.Open();
        this.jSPInfo.setText(SP.Check());
        SP.startCollecting();  
        jTotal.setEnabled(false);
        m_jKeys.setInactive(jTotal);
    }
    
    private void payOut(){
        SP.abortCollecting();
        m_dPaid = 0;
        jTotal.reset();
        jTotal.setEnabled(true);
        m_jKeys.doEnabled(true);
    }
            
    private void CancelPayment(){
        SP.abortCollecting();
        SP.Destroy();
    }
    
    private String selectMoneyFormat(double total){
        DispenseEditor dEditor = DispenseEditor.getDispenseEditor(null, m_App, SP, total);
        dEditor.setVisible(true);
        return dEditor.getSelectedFormat() == null ? null : dEditor.getSelectedFormat();
    }
    
    @Override
    public Component getComponent() {
        return this;
    }
    
    @Override
    public void update(Observable o, Object arg) {
        printState();
    }
    
    private void printState(){
        SP.ManageErrors();
        m_dPaid = SP.getDeposited();
        this.jTotal.setDoubleValue(m_dPaid);
                
        repaint();
        revalidate();
    }

    @Override
    public void refresh() {    }        
    
    private void setReasonTotal(Object reasonfield, Object totalfield) {
        m_ReasonModel.setSelectedKey(reasonfield);    
        PaymentReason reason = (PaymentReason) m_ReasonModel.getSelectedItem();     
        
        if (reason == null) {
            jTotal.setDoubleValue((Double) totalfield);
        } else {
            jTotal.setDoubleValue(reason.positivize((Double) totalfield));
        }  
    }
    
    private static abstract class PaymentReason implements IKeyed {
        private final String m_sKey;
        private final String m_sText;
        
        public PaymentReason(String key, String text) {
            m_sKey = key;
            m_sText = text;
        }
        @Override
        public Object getKey() {
            return m_sKey;
        }
        public abstract Double positivize(Double d);
        public abstract Double addSignum(Double d);
        
        @Override
        public String toString() {
            return m_sText;
        }
    }
    private static class PaymentReasonPositive extends PaymentReason {
        public PaymentReasonPositive(String key, String text) {
            super(key, text);
        }
        @Override
        public Double positivize(Double d) {
            return d;
        }
        @Override
        public Double addSignum(Double d) {
            if (d == null) {
                return null;
            } else if (d < 0.0) {
                return -d;
            } else {
                return d;
            }
        }
    }
    private static class PaymentReasonNegative extends PaymentReason {
        public PaymentReasonNegative(String key, String text) {
            super(key, text);
        }
        @Override
        public Double positivize(Double d) {
            return d == null ? null : -d;
        }
        @Override
        public Double addSignum(Double d) {
            if (d == null) {
                return null;
            } else if (d > 0.0) {
                return -d;
            } else {
                return d;
            }
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        m_jreason = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jTotal = new com.openbravo.editor.JEditorCurrency();
        m_jNotes = new com.openbravo.editor.JEditorString();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButtonProvider = new javax.swing.JButton();
        jChkBoxSelectPayment = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jSPInfo = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        m_jKeys = new com.openbravo.editor.JEditorKeys();

        setLayout(new java.awt.BorderLayout());

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText(AppLocal.getIntString("label.paymentreason")); // NOI18N

        m_jreason.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        m_jreason.setFocusable(false);

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText(AppLocal.getIntString("label.paymenttotal")); // NOI18N

        jTotal.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        m_jNotes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setText(AppLocal.getIntString("label.provider")); // NOI18N

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setText(AppLocal.getIntString("label.comment")); // NOI18N

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("pos_messages"); // NOI18N
        jButtonProvider.setText(bundle.getString("Label.SelectProvider")); // NOI18N
        jButtonProvider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonProviderActionPerformed(evt);
            }
        });

        jChkBoxSelectPayment.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jChkBoxSelectPayment.setText("Select Money");
        jChkBoxSelectPayment.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("SafePay Info:");

        jSPInfo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(m_jNotes, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                            .addComponent(m_jreason, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonProvider)
                            .addComponent(jTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jChkBoxSelectPayment))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSPInfo)))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(m_jreason, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(m_jNotes, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jChkBoxSelectPayment))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonProvider))
                .addGap(29, 29, 29)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSPInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(254, Short.MAX_VALUE))
        );

        add(jPanel3, java.awt.BorderLayout.CENTER);

        jPanel2.setLayout(new java.awt.BorderLayout());

        m_jKeys.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                m_jKeysPropertyChange(evt);
            }
        });
        jPanel2.add(m_jKeys, java.awt.BorderLayout.NORTH);

        add(jPanel2, java.awt.BorderLayout.LINE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void m_jKeysPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_m_jKeysPropertyChange

    }//GEN-LAST:event_m_jKeysPropertyChange

    private void jButtonProviderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonProviderActionPerformed
        JProviderFinder provList = JProviderFinder.getProviderFinder(this,dlProviders);
        provList.setVisible(true);
        provider = provList.getSelectedProvider();
        if (provider != null) {
            jButtonProvider.setText(provider.getName());
            jButtonProvider.validate();
        }  
    }//GEN-LAST:event_jButtonProviderActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonProvider;
    private javax.swing.JCheckBox jChkBoxSelectPayment;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel jSPInfo;
    private com.openbravo.editor.JEditorCurrency jTotal;
    private com.openbravo.editor.JEditorKeys m_jKeys;
    private com.openbravo.editor.JEditorString m_jNotes;
    private javax.swing.JComboBox m_jreason;
    // End of variables declaration//GEN-END:variables
    
}
