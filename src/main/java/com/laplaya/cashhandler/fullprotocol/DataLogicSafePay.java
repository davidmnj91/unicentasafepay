/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.laplaya.cashhandler.fullprotocol;

import com.openbravo.basic.BasicException;
import com.openbravo.data.loader.Datas;
import com.openbravo.data.loader.PreparedSentence;
import com.openbravo.data.loader.QBFBuilder;
import com.openbravo.data.loader.SentenceList;
import com.openbravo.data.loader.SerializerReadClass;
import com.openbravo.data.loader.SerializerWriteBasic;
import com.openbravo.data.loader.SerializerWriteBasicExt;
import com.openbravo.data.loader.SerializerWriteString;
import com.openbravo.data.loader.Session;
import com.openbravo.data.loader.StaticSentence;
import com.openbravo.pos.forms.BeanFactoryDataSingle;

/**
 *
 * @author ADMS
 */
public class DataLogicSafePay extends BeanFactoryDataSingle {
    
    private Session s;
    private static final Datas[] TRANSACTION = new Datas[] {
            Datas.STRING, 
            Datas.TIMESTAMP, 
            Datas.STRING, 
            Datas.DOUBLE
        };
    
    /**
     *
     * @param s
     */
    @Override
    public void init(Session s){
        this.s = s;
    }
         
    public SentenceList getTransactionList() {
         return new StaticSentence(s
            , new QBFBuilder(
            "SELECT SAFEPAY.ID, DATE, PEOPLE.NAME, TOTAL FROM SAFEPAY LEFT JOIN PEOPLE "
                 + "ON SAFEPAY.EMPLOYEE = PEOPLE.ID "
                 + "WHERE ?(QBF_FILTER) "
                 + "GROUP BY "
                 + "SAFEPAY.ID, "
                 + "DATE, "
                 + "PEOPLE.NAME "
                 + "ORDER BY DATE DESC", 
                 new String[] {"DATE", "DATE", "PEOPLE.NAME", "TOTAL"})
            , new SerializerWriteBasic(new Datas[] {
                Datas.OBJECT, Datas.TIMESTAMP, 
                Datas.OBJECT, Datas.TIMESTAMP,
                Datas.OBJECT, Datas.STRING, 
                Datas.OBJECT, Datas.DOUBLE})
            , new SerializerReadClass(TransactionInfo.class));
    }
    
    /**
     *
     * @param id
     * @param transact
     * @throws BasicException
     */
    public final void updateTransaction(final String id, final TransactionInfo transact) throws BasicException {
         
        Object[] values = new Object[] {
            id, 
            transact.getDate(), 
            transact.getEmployee(), 
            transact.getTotal()
        };
        
        new PreparedSentence(s
                , "UPDATE SAFEPAY SET "
                + "DATE = ?, "
                + "EMPLOYEE = ?, "
                + "TOTAL = ? "
                + "WHERE ID = ?"
                , new SerializerWriteBasicExt(TRANSACTION, new int[] {1, 2, 3, 0})).exec(values);
    }
    
    /**
     * @param transact
     * @throws BasicException
     */
    public final void insertTransaction(final TransactionInfo transact) throws BasicException {
        
        Object[] values = new Object[] {
            transact.getId(), 
            transact.getDate(), 
            transact.getEmployee(), 
            transact.getTotal()
        };
        
        new PreparedSentence(s
            , "INSERT INTO SAFEPAY ("
                + "ID, "
                + "DATE, "
                + "EMPLOYEE, "
                + "TOTAL) "
                + "VALUES (?, ?, ?, ?)"
            , new SerializerWriteBasicExt(TRANSACTION, new int[] {0, 1, 2, 3})).exec(values);
    }
    
    /**
     *
     * @param id
     * @throws BasicException
     */
    public final void deleteTransaction(final String id) throws BasicException {

        new StaticSentence(s
            , "DELETE FROM SAFEPAY WHERE ID = ?"
            , SerializerWriteString.INSTANCE).exec(id);      
    } 
}
