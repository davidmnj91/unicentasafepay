/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.laplaya.cashhandler.fullprotocol;

import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.AppView;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ADMS
 */
public class SafePay {
    
    private static final Integer TIMEOUT = 3;
    private final double MaxQuantity; 
    private final long ErrorMask;
    private final int TransType;
    private final TransactionInfo Transaction;
    private final DataLogicSafePay dlSafePay;
    private final AppView APP;
    private static SafePay INSTANCE = null;
    private static SafePayFunctions SPFP;
    private static mySafePayListener SPListener;
    
    private static final Logger logger = Logger.getLogger("UNICENTA");
    
    private SafePay(AppView app, int transtype, mySafePayListener SPListener){
        APP = app;
        TransType = transtype;
        if(!"".equals(app.getProperties().getProperty("payment.safepay.MaxQuantity"))){
            MaxQuantity = Double.parseDouble(app.getProperties().getProperty("payment.safepay.MaxQuantity"));
        }else{
            MaxQuantity = -1;
        }
        if(!"".equals(app.getProperties().getProperty("payment.safepay.ErrorMask"))){
            ErrorMask = Long.parseLong(app.getProperties().getProperty("payment.safepay.ErrorMask"));
        }else{
            ErrorMask = 0L;
        }

        SPFP = new SafePayFunctions(APP, SPListener, TransType);
        dlSafePay = (DataLogicSafePay) APP.getBean("com.laplaya.cashhandler.fullprotocol.DataLogicSafePay");
        Transaction = new TransactionInfo(dlSafePay, APP);
    }
    
    public synchronized static void createInstance(AppView app, int transtype){
        SPListener = new mySafePayListener();
        INSTANCE = new SafePay(app, transtype, SPListener);
    }
    
    public static SafePay getInstance(AppView app, int transtype){
        if(INSTANCE != null){
            INSTANCE.Destroy();
        }
        createInstance(app, transtype);
        return INSTANCE;
    }
    
    public void RegisterObserver(Observer o){
        SPListener.addObserver(o);
    }
    
    public void RemoveObserver(){
        SPListener.deleteObservers();
    }
    
    public void Open(){
        while(!Connection()){}
    }
    
    public double getDeposited(){ return SPListener.getDeposited(); }
    
    public String getUrgentMessage(){ return SPListener.getUrgentMessage(); }
    
    public void startCollecting(){
        if(SPFP.BeginDeposit() != 0){
            ManageErrors();
            Connection();
        }
    }
        
    public void endCollecting(){
        if(SPFP.GetDeposit() != 0){
            ManageErrors();
            Connection();
        }
    }
    
    public void abortCollecting(){
        if(SPFP.EndDeposit() != 0){
            ManageErrors();
            Connection();
        }
    }
    
    public boolean canDispense(double amount, String format){
        if((SPFP.SimulateDispense(amount, format) != 0) || (MaxQuantity > 0 && MaxQuantity < amount)){
            logger.log(Level.INFO, "{0} Error dispensing amount: " + amount + " in format: " + format, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            JOptionPane.showMessageDialog(null, "Amount Specified is unavaliable at the moment", "ERROR", JOptionPane.ERROR_MESSAGE);
            return false;
        }
        logger.log(Level.INFO, "{0} Success dispensing amount: " + amount + " in format: " + format, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
        return true;
    }
    
    public double Dispense(double amount, String Format){
        int i = 1; //Maximun number of attemps to avoid infinite loop
        double initialAmount = SPFP.double2cents(amount)/100;
        Transaction.createValue(amount);
        logger.log(Level.INFO, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + " Request to dispense: {0}", amount);
        while(SPFP.double2cents(amount)/100 > 0 || i > 5){
            amount -= SPFP.Dispense(amount, Format);
            Transaction.updateValue(amount);
            ManageErrors();
            if(initialAmount == amount) i++;
            logger.log(Level.INFO,new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()) + " Reaniming: {0}", amount);
        }
        Transaction.deleteValue();
        return (initialAmount - amount);
    }     
        
    public String Check(){
        if(SPFP.Check() == 0){
            logger.log(Level.INFO, "{0} Checking SafePay", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            List<String>Messages = new ArrayList();
            String message = "inicio";
            while(!"".equals(message)){
                message = SPFP.getMessage();
                if(!"".equals(message)) Messages.add(message);
            }
            logger.log(Level.INFO, "{0} Check Success", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            return toHTML(Messages);
        } else {
            logger.log(Level.INFO, "{0} Check Failed", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            return "SafePay Check Failed!!";
        }
    }
    
    public void Close(){
        SPFP.Disconnect();
    }
    
    public void Destroy(){
        logger.log(Level.INFO, "{0} Closing SafePay", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));            
        while(SPFP.isOpen()){
            logger.log(Level.INFO, "{0} Clossing SafePay Attemp: 1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            RemoveObserver();
            SPFP.Disconnect();
        }
        logger.log(Level.INFO, "{0} SafePay Closed", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
        INSTANCE = null;
    }
    
    public Map<Double,Integer>CurrenciesTracking(){
        return SPFP.CurrenciesTracking();
    } 
    
    private boolean Connection(){
        boolean result = false;
        while (!result){
            try {
                result = TimeLimitedCodeBlock.runWithTimeout(() -> SPFP.Connect(), TIMEOUT, TimeUnit.SECONDS);
                logger.log(Level.INFO, "{0} Trying to connect with SafePay " + result, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            }catch (TimeoutException e) { } catch (Exception ex) { }
            if(!result){
                if (!showError(AppLocal.getIntString("message.SafePayReconnection"))) {
                    logger.log(Level.INFO, "{0} Cancel connection button pressed", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                    JOptionPane.showMessageDialog(null, AppLocal.getIntString("message.UnicentaClosing"),"ERROR!",JOptionPane.ERROR_MESSAGE);
                    logger.log(Level.INFO, "{0} Unicenta will close with error", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                    System.exit(-1);
                }
            }
        }
        return true;
    }
    
    public void OpeningCover(){
        if(SPFP.isAlive()){
            if(SPFP.OpenCover()){
                JOptionPane.showMessageDialog(null, AppLocal.getIntString("message.CoverOpen"), "WARNING!", JOptionPane.INFORMATION_MESSAGE);
               // ManageErrors();
            }
        }
    }
    
    public void ReleaseCassete(){
        if(SPFP.ReleaseCasette() == 6){
            JOptionPane.showMessageDialog(null, "Certificate needed", "INFO:", JOptionPane.INFORMATION_MESSAGE);
        } 
    }
    
    public void NoteTransfer(short mode){
        if (!SPFP.NoteTransfer(mode)){
            JOptionPane.showMessageDialog(null, "Note Transfer Failed", "INFO:", JOptionPane.INFORMATION_MESSAGE);
        }
    }
         
    private String CheckAmounts(double amount){
        String res = "";
        double total = 0;
        TreeMap <Double,Integer> Currencies = new TreeMap<>(CurrenciesTracking());
        total = Currencies.entrySet().stream().map((entry) -> entry.getKey() * entry.getValue()).reduce(total, (accumulator, _item) -> accumulator + _item);
        if(total >= amount){
            while(amount > 0){
                if(((int)(amount / Currencies.floorKey(amount))) <= Currencies.get(Currencies.floorKey(amount))){
                    res += (Currencies.floorKey(amount) * 100) + ":" + (int)(amount / Currencies.floorKey(amount)) + ";";
                    amount = amount % Currencies.floorKey(amount);
                } else {
                    res += (Currencies.floorKey(amount) * 100) + ":" + Currencies.get(Currencies.floorKey(amount)) + ";";
                    amount = amount - (Currencies.floorKey(amount) * Currencies.get(Currencies.floorKey(amount)));
                    Currencies.remove(Currencies.floorKey(amount));
                }
            }
            return res.substring(0, res.length()-1);
        }
        return null;
    }
    
    public void ManageErrors() {
        String s = getUrgentMessage();
        if(s != null){
            JOptionPane.showMessageDialog(null, s);
            logger.log(Level.INFO, "{0} Error in SafePay: " + s, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));            
        }    
        while((ErrorMask & Long.parseLong(SPListener.getStatus(),2)) != 0){
            logger.log(Level.INFO, "{0} Error in SafePay: " + s, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            JOptionPane.showMessageDialog(null, s);
        }    
    }
       
    private boolean showError(String error){
        int i = JOptionPane.showConfirmDialog(null, error, "INFO!", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        return i == JOptionPane.OK_OPTION;
    }
    
    private String toHTML(List<String> l){
        String res = "";
        res = l.stream().map((s) -> "<li>"+s+"</li>").reduce(res, String::concat)+"</ul></html>";
        return "<html><ul>"+res;    
    }
    
    public double roundDouble(double POSval) {
        double cents;
        BigDecimal v1 = new BigDecimal(POSval).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal v2 = new BigDecimal(v1.toPlainString());
        cents = v2.movePointRight(2).intValue();
        return cents/100;
    }
}
