/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.laplaya.cashhandler.fullprotocol;

import com.gunnebo.safepay.pos.SafePayListener;
import java.awt.EventQueue;
import java.util.Observable;

/**
 *
 * @author ADMS
 */
public class mySafePayListener extends Observable implements SafePayListener {
    
    private volatile String Status;
    private volatile String Message;
    private volatile double Deposited;
    private volatile double Dispensed;
        
    public mySafePayListener() {
        super();
    }
    
    public void notifyAllObservers(Object arg){
        EventQueue.invokeLater(() -> {
            notifyObservers(arg);
        });
    }
        
    private synchronized void setStatus(int status){
        this.Status = Integer.toBinaryString(status);
        setChanged();
        //notifyAllObservers(Status);
    }
    
    public synchronized String getStatus(){
        return Status; 
    }
    
    private synchronized void setDeposited(int amount){
        this.Deposited = ((double)amount/100);
        setChanged();
        notifyAllObservers(Deposited);
    }
    
    public synchronized double getDeposited(){ return Deposited; }
    
    private void setDispensed(int amount){
        this.Dispensed = ((double)amount/100);
        setChanged();
        notifyAllObservers(Dispensed);
    }
    
    public double getDispensed(){ return Dispensed; }
    
    private synchronized void setUrgentMessage(String msg){
        this.Message = msg;
        setChanged();
        notifyAllObservers(Message);
    }
    
    public synchronized String getUrgentMessage(){
        String s = this.Message;
        this.Message = null;
        return s;
    }
    
    @Override
    public void handleDeviceStatus(int status) {
        setStatus(status);
    }

    @Override
    public void handleDepositAmount(int amount) {
        setDeposited(amount);
    }

    @Override
    public void handleDispenseAmount(int amount) {
        setDispensed(amount);
    }

    @Override
    public void handleInventoryList(String list) {}

    @Override
    public void handleUrgentMessage(String msg) {
        setUrgentMessage(msg);
    } 
}
