/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.laplaya.cashhandler.fullprotocol;

import com.openbravo.basic.BasicException;
import com.openbravo.data.loader.DataRead;
import com.openbravo.data.loader.SerializableRead;
import com.openbravo.data.loader.SerializerRead;
import com.openbravo.format.Formats;
import com.openbravo.pos.forms.AppView;
import com.openbravo.pos.forms.DataLogicSales;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMS
 */
public class TransactionInfo implements SerializableRead {

    /**
     * Transaction unique ID
     */
    protected String id;
    

    /**
     *Transaction Date
     */
    protected Date date;

    /**
     * Transaction Employee
     */
    protected String employee;

    /**
     * Transaction total value
     */
    protected double total;

    private AppView app;
    
    private DataLogicSafePay dlSafePay;
    
    /** Creates a new instance of UserInfoBasic
     **/
    public TransactionInfo() {  
    }
    
    public TransactionInfo(DataLogicSafePay dlSafePay, AppView app){  
        this.dlSafePay = dlSafePay;
        this.app = app;
    }
    
    public TransactionInfo createTransactionInfo(double total){
        this.id = UUID.randomUUID().toString();
        this.date = new Date();
        this.employee = app.getAppUserView().getUser().getId();
        this.total = total;
        
        return this;
    }
    
    public TransactionInfo(String id, Date date, String employee, Double total) {
        this.id = id;
        this.date = date;
        this.employee = employee;
        this.total = total;
    }
    
    /**
     *
     * @return id string
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @return name string
     */
    public Date getDate() {
        return date;
    }   

    /**
     *
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     * @return postal/zip code string
     */
    public String getEmployee() {
        return employee;
    }   

    /**
     *
     * @param employee
     */
    public void setEmployee(String employee) {
        this.employee = employee;
    }
    
    /**
     *
     * @return Primary Telephone string
     */
    public Double getTotal() {
        return total;
    }

    /**
     *
     * @param total
     */
    public void setTotal(double total) {
        this.total = total;
    }
    
    public void createValue (Double total){
        try {
            dlSafePay.insertTransaction(createTransactionInfo(total));
        } catch (BasicException ex) {
            Logger.getLogger(TransactionInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateValue (Double total){
        this.date = new Date();
        this.total = total;
        try {
            dlSafePay.updateTransaction(this.id, this);
        } catch (BasicException ex) {
            Logger.getLogger(TransactionInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteValue(){
        try {
            dlSafePay.deleteTransaction(this.id);
        } catch (BasicException ex) {
            Logger.getLogger(TransactionInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     *
     * @return ticketlines for this provider
     */
    public static SerializerRead getSerializerRead() {
        return (DataRead dr) -> {
            String id = dr.getString(1);
            String Date = dr.getString(2);
            String Employee = dr.getString(3);
            Double Total = dr.getDouble(4);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = null;
            try {
                date = formatter.parse(Date);
            }catch (ParseException ex) {
                Logger.getLogger(DataLogicSales.class.getName()).log(Level.SEVERE, null, ex);
            }
            return new TransactionInfo(id, date, Employee, Total);
        };
    }

    @Override
    public void readValues(DataRead dr) throws BasicException {
        id = dr.getString(1);
        date = dr.getTimestamp(2);
        employee = dr.getString(3);
        total = (dr.getObject(4) == null) ? 0.0 : dr.getDouble(4);
    }
    
    @Override
    public String toString(){
        String sHtml = "<tr><td width=\"50\">"+ "["+ id +"]" +"</td>" +
                "<td width=\"100\">"+ Formats.TIMESTAMP.formatValue(date) +"</td>" +
                "<td align=\"center\" width=\"100\">"+ employee +"</td>" +
                "<td align=\"right\" width=\"100\">"+ Formats.CURRENCY.formatValue(total) +"</td></tr>";
        
        return sHtml;
    }
}
