/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.laplaya.cashhandler.fullprotocol;

import com.gunnebo.safepay.pos.*;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.AppView;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author AMDS
 */
class SafePayFunctions {
    
    private final SafePaySP SP;
    private final mySafePayListener Handler;

    private final String User;
    private final String IP;
    private final String Port;
    private final String Cur;
    private final String Currency[] = {""};
    private final int TransType;
    
    SafePayFunctions(AppView app, mySafePayListener SPListener, int transtype) {
        SP = new SafePaySP();
        this.Handler = SPListener;
        this.IP = app.getProperties().getProperty("payment.safepay.IP"); 
        this.Port = app.getProperties().getProperty("payment.safepay.Port");
        this.User = app.getAppUserView().getUser().getName();
        this.Cur = app.getProperties().getProperty("format.currency");
        this.Currency[0] = Cur;
        this.TransType = transtype;
    }
        
    boolean Connect() {         
        if(!isOpen()){
            DateFormat dateFormat = new SimpleDateFormat("YYYYMMddHHmmss");
            Date date = new Date();
            String CurrentDate = dateFormat.format(date);  

            String ConectionString = "cha:tcp;addr:"+this.IP+";port:"+this.Port+";";
            return SP.ProtocolSelect(ConectionString) == 0 &&
                SP.ProtocolStart() > 0 &&
                SP.RegisterEventHandler(this.Handler, 46) == 0 &&
                SP.Open(User, CurrentDate) == 0;
        }
        return true;
    }
    
    String StatusTracking(){
        int status[] = {0};
        SP.Status(status);
        return Integer.toBinaryString(status[0]);
    }
    
    public boolean isOpen(){
        return StatusTracking().charAt(0) == '1';
    }
    
    public boolean isAlive(){
        String status = StatusTracking();
        return status.length() > 1 || status.length() < 16;
    }
    
    int BeginDeposit(){
        return SP.BeginDeposit(Currency);
    }
    
    boolean Deposit() {
        int given[] = {0};
        return SP.Deposit(given, null, Currency) == 0;
    }
    
    int GetDeposit(){
        int given[] = {0};
        return SP.GetDeposit(given, (byte)TransType, null, Currency);
    }
    
    int EndDeposit(){
        return SP.EndDeposit();
    }
    
    int SimulateDispense(double amount, String Notes){
        int Amount[] = {amount > 0 ? (int)(double2cents(amount)) : 0};
        String[] notes = {Notes};
        return SP.SimulateDispense(Amount, notes, Currency);
    }
    
    double Dispense(double amount, String Notes){
        int Amount[] = {amount > 0 ? (int)(double2cents(amount)) : 0};
        String[] notes = {Notes};
        if(SP.Dispense(Amount, (byte)TransType, notes, Currency) == 0){
            return ((double)Amount[0]/100);
        }
        return 0;
    }
    
    int Check(){
        return SP.Check();
    }
    
    boolean Disconnect(){
        return SP.Close() == 0 && SP.ProtocolStop() == 0;
    }
    
    String getMessage(){
        String msg[] = {null};
        if(SP.Message(msg) == 0) return msg[0];
        return "";
    }
    
    boolean OpenCover(){
        return SP.OpenCover((byte)1) == 0;
    }
    
    int ReleaseCasette() {
        return SP.ReleaseCassette((byte)0);
    }
    
    Map<Double,Integer> CurrenciesTracking(){
        String Cash[] = {""};
        SP.GetCashCount(Cur, Cash);
        String[] s = Cash[0].replace(";", ",").split(",");
        Map<Double,Integer> MoneyInfo = new HashMap<>();
        for (String item : s) {
            String[] num = item.split(":");
            try{
                MoneyInfo.putIfAbsent(Double.parseDouble(num[0])/100, Integer.valueOf(num[1])); 
            }catch(NumberFormatException e){}
        }
        return MoneyInfo;
    }
    
    boolean NoteTransfer(short mode){
        return SP.NoteTransfer(mode) == 0;
    }
    
    List<String> Exception(String status){ //Checkear errores asociacion mensajes
        List<String> errors = new ArrayList();
        errors.clear();
        if(status == null || (status.length() == 1)){ 
            errors.add(AppLocal.getIntString("safepay.connected.ERROR"));
            return errors;
        } else if (status.length() > 1 && status.length() < 16){
            return errors;
        } else{
            for (int i = 16; i<status.length();i++) {
                if (status.charAt(i)=='1'){
                    switch (i) {
                        case 16:
                            errors.add(AppLocal.getIntString("message.SafePayError16"));
                            break;
                        case 17:
                            errors.add(AppLocal.getIntString("message.SafePayError17"));
                            break;
                        case 18:
                            errors.add(AppLocal.getIntString("message.SafePayError18"));
                            break;
                        case 19:
                            errors.add(AppLocal.getIntString("message.SafePayError19"));
                            break;
                        case 20:
                            errors.add(AppLocal.getIntString("message.SafePayError20"));
                            break;
                        case 21:
                            errors.add(AppLocal.getIntString("message.SafePayError21"));
                            break;
                        case 23:
                            errors.add(AppLocal.getIntString("message.SafePayError23"));
                            break;
                        case 24:
                            errors.add(AppLocal.getIntString("message.SafePayError24"));
                            break;
                        case 25:
                            errors.add(AppLocal.getIntString("message.SafePayError25"));
                            break;
                        case 26:
                            errors.add(AppLocal.getIntString("message.SafePayError26"));
                            break;
                        case 27:
                            errors.add(AppLocal.getIntString("message.SafePayError27"));
                            break;
                        case 28:
                            errors.add(AppLocal.getIntString("message.SafePayError28"));
                            break;
                        case 29:
                            errors.add(AppLocal.getIntString("message.SafePayError29"));
                            break;
                        case 30:
                            errors.add(AppLocal.getIntString("message.SafePayError30"));
                            break;
                        case 31:
                            errors.add(AppLocal.getIntString("message.SafePayError31"));
                            break;
                    }
                }
            }
        }
        return errors;
    }
    
    double double2cents(double POSval) {
        double cents;
        BigDecimal v1 = new BigDecimal(POSval).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal v2 = new BigDecimal(v1.toPlainString());
        cents = v2.movePointRight(2).intValue();
        return cents;
    } 
}