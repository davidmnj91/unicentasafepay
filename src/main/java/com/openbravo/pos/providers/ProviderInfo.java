/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openbravo.pos.providers;

import com.openbravo.pos.util.StringUtils;
import java.io.Serializable;

/**
 *
 * @author David
 */
public class ProviderInfo implements Serializable{
    private static final long serialVersionUID = 9083257536541L;

    /**
     * Customer unique ID
     */
    protected String id;
    

    /**
     *Customer Account Name
     */
    protected String name;

    /**
     * Customer post/zip code
     */
    protected String postal;

    /**
     * Customer Primary telephone
     */
    protected String phone;

    /**
     * Customer Email
     */
    protected String email;    
    
    /** Creates a new instance of UserInfoBasic
     * @param id */
    public ProviderInfo(String id) {
        this.id = id;
        this.name = null;
        this.postal = null;
        this.phone = null;
        this.email = null;
    }
    
    /**
     *
     * @return id string
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @return name string
     */
    public String getName() {
        return name;
    }   

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return postal/zip code string
     */
    public String getPostal() {
        return postal;
    }   

    /**
     *
     * @param postal
     */
    public void setPostal(String postal) {
        this.postal = postal;
    }
    
    /**
     *
     * @return Primary Telephone string
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    /**
     *
     * @return email string
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    public String printName() {
        return StringUtils.encodeXML(name);
    }
    
    @Override
    public String toString() {
        return getName();
    }    
}
