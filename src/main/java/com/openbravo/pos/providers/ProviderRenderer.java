/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openbravo.pos.providers;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JList;

/**
 *
 * @author David
 */
public class ProviderRenderer extends DefaultListCellRenderer {
    private Icon icoprovider;

    /** Creates a new instance of CustomerRenderer */
    public ProviderRenderer() {

        icoprovider = new ImageIcon(getClass().getClassLoader().getResource("com/openbravo/images/customer_sml.png"));
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, null, index, isSelected, cellHasFocus);
        setText(value.toString());
        setIcon(icoprovider);
        return this;
    } 
}
