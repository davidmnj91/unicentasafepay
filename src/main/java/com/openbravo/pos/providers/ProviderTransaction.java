/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openbravo.pos.providers;

import com.openbravo.basic.BasicException;
import com.openbravo.data.loader.DataRead;
import com.openbravo.data.loader.SerializerRead;
import com.openbravo.pos.forms.DataLogicSales;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author David
 */
public class ProviderTransaction {
    String providerName;
    String tender;
    String notes;
    Double total;
    Date date;
    /**
     * Main method to return all provider's transactions 
     */
    public ProviderTransaction() {
    }

    /**
     *
     * @param total
     */
    public ProviderTransaction(String providerName, Date date, String tender, String notes, Double total) {
        this.providerName = providerName;
        this.date = date;
        this.tender = tender;
        this.notes = notes;
        this.total = total;
    }

    /**
     *
     * @return ticket id string
     */
    public String getProviderName() {
        return providerName;
    }

    /**
     *
     * @param ticketId
     */
    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    /**
     *
     * @return ticket amount value
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param amount
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     * @param total
     */
    public void setTender(String  tender) {
        this.tender = tender;
    }

    /**
     *
     * @return ticketline value
     */
    public String getTender() {
        return tender;
    }
    
    /**
     *
     * @return ticketline's product name string 
     */
    public String getNotes() {
        return notes;
    }

    /**
     *
     * @param productName
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     *
     * @return ticket's transaction date
     */
    public Double getTotal() {
        return total;
    }

    /**
     *
     * @param transactionDate
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     *
     * @return ticketlines for this provider
     */
    public static SerializerRead getSerializerRead() {
        return new SerializerRead() {

            @Override
            public Object readValues(DataRead dr) throws BasicException {

                String providerName = dr.getString(1);
                String Date = dr.getString(2);
                String Employee = dr.getString(3);
                String Notes = dr.getString(4);
                Double Total = dr.getDouble(5);
                
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                Date date = null;
                try {
                    date = formatter.parse(Date);
                } catch (ParseException ex) {
                    Logger.getLogger(DataLogicSales.class.getName()).log(Level.SEVERE, null, ex);
                }
                return new ProviderTransaction(providerName, date, Employee, Notes, Total);
            }
        };
    }
}
