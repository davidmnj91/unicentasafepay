/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openbravo.pos.providers;

import com.openbravo.data.user.BrowsableEditableData;

/**
 *
 * @author David
 */
public class ProviderInfoGlobal {
    private static ProviderInfoGlobal INSTANCE;
    private ProviderInfoExt providerInfoExt;
    private BrowsableEditableData editableData;

    //Singleton class
    private ProviderInfoGlobal() {
    }

    //Singleton constructor

    /**
     *
     * @return
     */
        public static ProviderInfoGlobal getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProviderInfoGlobal();
        }

        return INSTANCE;
    }

    /**
     *
     * @return
     */
    public ProviderInfoExt getProviderInfoExt() {
        return providerInfoExt;
    }

    /**
     *
     * @param providerInfoExt
     */
    public void setProviderInfoExt(ProviderInfoExt providerInfoExt) {
        this.providerInfoExt = providerInfoExt;
    }

    /**
     *
     * @return
     */
    public BrowsableEditableData getEditableData() {
        return editableData;
}

    /**
     *
     * @param editableData
     */
    public void setEditableData(BrowsableEditableData editableData) {
        this.editableData = editableData;
    }
}
