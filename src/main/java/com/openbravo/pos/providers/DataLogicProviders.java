/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openbravo.pos.providers;

import com.openbravo.basic.BasicException;
import com.openbravo.data.loader.DataParams;
import com.openbravo.data.loader.DataRead;
import com.openbravo.data.loader.Datas;
import com.openbravo.data.loader.PreparedSentence;
import com.openbravo.data.loader.QBFBuilder;
import com.openbravo.data.loader.SentenceExec;
import com.openbravo.data.loader.SentenceExecTransaction;
import com.openbravo.data.loader.SentenceList;
import com.openbravo.data.loader.SerializerRead;
import com.openbravo.data.loader.SerializerReadBasic;
import com.openbravo.data.loader.SerializerWriteBasic;
import com.openbravo.data.loader.SerializerWriteBasicExt;
import com.openbravo.data.loader.SerializerWriteParams;
import com.openbravo.data.loader.Session;
import com.openbravo.data.loader.StaticSentence;
import com.openbravo.data.loader.TableDefinition;
import com.openbravo.format.Formats;
import com.openbravo.pos.forms.AppLocal;
import com.openbravo.pos.forms.BeanFactoryDataSingle;

/**
 *
 * @author David
 */
public class DataLogicProviders extends BeanFactoryDataSingle {
    
    /**
     * Main Method for Provider object
     */
    protected Session s;
    private TableDefinition tproviders;
    private static final Datas[] providerdatas = new Datas[] {
        Datas.STRING, 
        Datas.TIMESTAMP, 
        Datas.TIMESTAMP, 
        Datas.STRING, 
        Datas.STRING, 
        Datas.STRING, 
        Datas.STRING, 
        Datas.INT, 
        Datas.BOOLEAN, 
        Datas.STRING};
    
    /**
     *
     * @param s
     */
    @Override
    public void init(Session s){
// JG 03 Oct - Added Provider Image        
        this.s = s;
        tproviders = new TableDefinition(s
            , "PROVIDERS"
            , new String[] { 
                "ID",  
                "NAME", 
                "NOTES", 
                "VISIBLE", 
                "CARD", 
                "FIRSTNAME",
                "LASTNAME",
                "EMAIL",
                "PHONE",
                "PHONE2",
                "FAX",
                "ADDRESS",
                "ADDRESS2",
                "POSTAL",
                "CITY",
                "REGION",
                "COUNTRY",
                "CATEGORY",
                "IMAGE" }
            , new String[] { 
                "ID", 
                AppLocal.getIntString("label.name"),
                AppLocal.getIntString("label.notes"),
                "VISIBLE",
                "CARD",
                AppLocal.getIntString("label.firstname"),
                AppLocal.getIntString("label.lastname"),
                AppLocal.getIntString("label.email"),
                AppLocal.getIntString("label.phone"),
                AppLocal.getIntString("label.phone2"),
                AppLocal.getIntString("label.fax"),
                AppLocal.getIntString("label.address"),
                AppLocal.getIntString("label.address2"),
                AppLocal.getIntString("label.postal"),
                AppLocal.getIntString("label.city"),
                AppLocal.getIntString("label.region"),
                AppLocal.getIntString("label.country"),
                "CATEGORY",
                "IMAGE" }
            , new Datas[] { 
                Datas.STRING, 
                Datas.STRING, 
                Datas.STRING,
                Datas.BOOLEAN, 
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.STRING,
                Datas.IMAGE }
            , new Formats[] {
                Formats.STRING, 
                Formats.STRING, 
                Formats.STRING, 
                Formats.BOOLEAN,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING,
                Formats.STRING }
            , new int[] {0}
        );
    }
    
// JG 20 Sept 12 extended for Postal - ProviderList list
// JG 2 Sept 13 extended for Phone + Email - ProviderList list

    /**
     *
     * @return provider data
    */
    //JG July 2014 - Amend to PHONE from PHONE2
        public SentenceList getProviderList() {
        return new StaticSentence(s
            , new QBFBuilder("SELECT ID, NAME FROM PROVIDERS WHERE VISIBLE = " + s.DB.TRUE() + " AND ?(QBF_FILTER) ORDER BY NAME", new String[] {"NAME"})
            , new SerializerWriteBasic(new Datas[] {  
                Datas.OBJECT, Datas.STRING})
            , (DataRead dr) -> {
                ProviderInfo p = new ProviderInfo(dr.getString(1));
                p.setName(dr.getString(2));
                
                return p;
        });
    }
       
    /**
     *
     * @param provider
     * @return
     * @throws BasicException
     */
    public int updateProviderExt(final ProviderInfoExt provider) throws BasicException {
     
        return new PreparedSentence(s
                , "UPDATE PROVIDERS SET NOTES = ? WHERE ID = ?"
                , SerializerWriteParams.INSTANCE      
                ).exec(new DataParams() {@Override
        public void writeValues() throws BasicException {
                        setString(1, provider.getNotes());
                        setString(2, provider.getId());
                }});        
    }
    
    /**
     *
     * @return Provider's existing reservation (restaurant mode)
     */
    public final SentenceList getReservationsList() {
        return new PreparedSentence(s
            , "SELECT R.ID, R.CREATED, R.DATENEW, P.PROVIDER, COALESCE(PROVIDERS.NAME, R.TITLE),  R.CHAIRS, R.ISDONE, R.DESCRIPTION " +
              "FROM RESERVATIONS R LEFT OUTER JOIN RESERVATION_PROVIDERS C ON R.ID = C.ID LEFT OUTER JOIN PROVIDERS ON P.PROVIDER = PROVIDERS.ID " +
              "WHERE R.DATENEW >= ? AND R.DATENEW < ?"
            , new SerializerWriteBasic(new Datas[] {Datas.TIMESTAMP, Datas.TIMESTAMP})
            , new SerializerReadBasic(providerdatas));             
    }
    
    /**
     *
     * @return create/update Provider reservation  (restaurant mode)
     */
    public final SentenceExec getReservationsUpdate() {
        return new SentenceExecTransaction(s) {
            @Override
            public int execInTransaction(Object params) throws BasicException {  
    
                new PreparedSentence(s
                    , "DELETE FROM RESERVATION_PROVIDERS WHERE ID = ?"
                    , new SerializerWriteBasicExt(providerdatas, new int[]{0})).exec(params);
                if (((Object[]) params)[3] != null) {
                    new PreparedSentence(s
                        , "INSERT INTO RESERVATION_PROVIDERS (ID, PROVIDER) VALUES (?, ?)"
                        , new SerializerWriteBasicExt(providerdatas, new int[]{0, 3})).exec(params);                
                }
                return new PreparedSentence(s
                    , "UPDATE RESERVATIONS SET ID = ?, CREATED = ?, DATENEW = ?, TITLE = ?, CHAIRS = ?, ISDONE = ?, DESCRIPTION = ? WHERE ID = ?"
                    , new SerializerWriteBasicExt(providerdatas, new int[]{0, 1, 2, 6, 7, 8, 9, 0})).exec(params);
            }
        };
    }
    
    /**
     *
     * @return delete Provider reservation (restaurant mode)
     */
    public final SentenceExec getReservationsDelete() {
        return new SentenceExecTransaction(s) {
            @Override
            public int execInTransaction(Object params) throws BasicException {  
    
                new PreparedSentence(s
                    , "DELETE FROM RESERVATION_PROVIDERS WHERE ID = ?"
                    , new SerializerWriteBasicExt(providerdatas, new int[]{0})).exec(params);
                return new PreparedSentence(s
                    , "DELETE FROM RESERVATIONS WHERE ID = ?"
                    , new SerializerWriteBasicExt(providerdatas, new int[]{0})).exec(params);
            }
        };
    }
    
    /**
     *
     * @return insert a new Provider reservation (restaurant mode)
     */
    public final SentenceExec getReservationsInsert() {
        return new SentenceExecTransaction(s) {
            @Override
            public int execInTransaction(Object params) throws BasicException {  
    
                int i = new PreparedSentence(s
                    , "INSERT INTO RESERVATIONS (ID, CREATED, DATENEW, TITLE, CHAIRS, ISDONE, DESCRIPTION) VALUES (?, ?, ?, ?, ?, ?, ?)"
                    , new SerializerWriteBasicExt(providerdatas, new int[]{0, 1, 2, 6, 7, 8, 9})).exec(params);

                if (((Object[]) params)[3] != null) {
                    new PreparedSentence(s
                        , "INSERT INTO RESERVATION_PROVIDERS (ID, PROVIDER) VALUES (?, ?)"
                        , new SerializerWriteBasicExt(providerdatas, new int[]{0, 3})).exec(params);                
                }
                return i;
            }
        };
    }
    
    /**
     *
     * @return assign a table to a Provider reservation (restaurant mode)
     */
    public final TableDefinition getTableProviders() {
        return tproviders;
    }  
    
}
