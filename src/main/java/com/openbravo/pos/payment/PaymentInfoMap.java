//    uniCenta oPOS  - Touch Friendly Point Of Sale
//    Copyright (c) 2009-2015 uniCenta
//    http://www.unicenta.com
//
//    This file is part of uniCenta oPOS
//
//    uniCenta oPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   uniCenta oPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with uniCenta oPOS.  If not, see <http://www.gnu.org/licenses/>.

package com.openbravo.pos.payment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author ADMS uniCenta
 */
public class PaymentInfoMap {
    
    private final Map<String,ArrayList<PaymentInfo>> m_apayment;
    
    /** Creates a new instance of PaymentInfoComposed */
    public PaymentInfoMap() {
// JG 16 May 12 use diamond inference
        m_apayment = new HashMap<>();
    }
        
    /**
     *
     * @return the global total iterate over all payment types and sum all the arralist
     */
    public double getTotal() {
        double dTotal = 0.0;
        for(ArrayList<PaymentInfo> list : m_apayment.values()){
            for(PaymentInfo p : list){
                dTotal += p.getTotal();
            }
        }       
        return dTotal;
    }
    
    /**
     *
     * @param key
     * @return the global of a given paymentby summing all the array list
     */
    public double getTotal(String key) {
        double dTotal = 0.0;
        return m_apayment.get(key).stream().map((p) -> p.getTotal()).reduce(dTotal, (accumulator, _item) -> accumulator + _item);
    }

    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return m_apayment.isEmpty();
    }
    
    /**
     *
     * @param key
     * @return
     */
    public boolean isEmpty(String key) {
        return m_apayment.get(key)!= null && !m_apayment.get(key).isEmpty();
    }
    
    /**
     *
     * @param key
     * @param p
     */
    public void add(String key, PaymentInfo p) {
        ArrayList<PaymentInfo> temp = new ArrayList<>();
        if(m_apayment.containsKey(key)) temp = m_apayment.get(key);
        temp.add(p);
        m_apayment.put(key, temp);
    }
    
    public PaymentInfo getLast(String key){
        return m_apayment.get(key).get(m_apayment.get(key).size()-1);
    }
    
    /**
     *
     * @param key
     */
    public void removeLast(String key) {
        ArrayList<PaymentInfo> temp = m_apayment.get(key);
        temp.remove(temp.size()-1);
        if(m_apayment.get(key).isEmpty()){
            m_apayment.remove(key);
        }else{
            m_apayment.put(key, temp);
        }
    }
    
    /**
     *
     * @return
     */
    public LinkedList<PaymentInfo> getPayments() {
        LinkedList<PaymentInfo> lista = new LinkedList<>();
        m_apayment.values().forEach((list) -> {lista.addAll(list);});
        return lista;
    }
    
    public Map<String,ArrayList<PaymentInfo>> getMap(){
        return m_apayment;
    }
    
    public String getPaymentsInfo(){
        String res = "";
        for(String key : m_apayment.keySet()){
            res+="<b><li>"+key+": </b>";
            String list ="";
            list=m_apayment.get(key).stream().map((s) -> s.printTotal()+", ").reduce(list, String::concat);
            res += list.substring(0, list.length()-2)+"</li>";
        }
        return "<html><ul>"+res+"</ul></html>";
    }
}
